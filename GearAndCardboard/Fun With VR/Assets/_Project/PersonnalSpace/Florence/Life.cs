﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace Interface3.Florence {
    public class Life : MonoBehaviour
    {
        private static string[] _femNameTable = new string[] { "Nina", "Joanne", "Arianne" };
        private static string[] _malNameTable = new string[] { "Georges", "Rick", "John" };




        [Header("About the player")]

        public string _name;

        [Range(0, 100)]
        public int _life = 100;

        [SerializeField]
        public enum possibleGenders { Other, Female, Male };
        public possibleGenders _gender;

        [HideInInspector]
        public UnityEvent _onPlayerDeath;

        [System.Serializable]
        public class OnLiveChangeEvent : UnityEvent<int> { }  
        public OnLiveChangeEvent _onLifeChange;

        public delegate void DisplayNameOfTheDeath(string name);

        public DisplayNameOfTheDeath _onPeopleDeath;


        // Use this for initialization
        void Start()
        {
            checkName();
            _onPlayerDeath.AddListener(SaidTheNameWhenDeath);
            _onPeopleDeath += SaidTheNameWhenDeath;
        }

        public void TakeDommage(int dommage) {

            _life -= dommage;
            _onLifeChange.Invoke(_life);
        }


        public void OnValidate()
        {

            _onLifeChange.Invoke(_life);
        }

        public void SaidTheNameWhenDeath()
        {
            Debug.Log(_name + " is death !");
        }
        public void SaidTheNameWhenDeath(string name)
        {
            Debug.Log(name + " is death !");
        }

        // Update is called once per frame
        void Update()
    {
        if (_life <= 0)
        {
                _onPlayerDeath.Invoke();
                _onPeopleDeath(_name);
            Destroy(gameObject);
        }
    }

    private void checkName()
    {
        if (_name == "")
        {
            randomName();
        }
    }

    public void randomName()
    {
        if (_gender == possibleGenders.Female)
        {
            _name = _femNameTable[Random.Range(0, _femNameTable.Length)];
        }
        if (_gender == possibleGenders.Male)
        {
            _name = _malNameTable[Random.Range(0, _malNameTable.Length)];
        }
        if (_gender == possibleGenders.Other)
        {
            int r = Random.Range(0, 2);
            if (r == 0)
            {
                _name = _malNameTable[Random.Range(0, _malNameTable.Length)];
            }
            else if (r == 1)
            {
                _name = _femNameTable[Random.Range(0, _femNameTable.Length)];
            }
            else
            {
                _name = "error name";
            }
        }
    }
}
}