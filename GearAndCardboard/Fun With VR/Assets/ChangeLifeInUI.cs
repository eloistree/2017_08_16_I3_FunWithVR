﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChangeLifeInUI : MonoBehaviour {

    public Interface3.Florence.Life _life;
    public Image _imageToAffect;
    // Use this for initialization

    void Start()
    {
        _life._onLifeChange.AddListener(ChangeTheLifeOfTheUser);

    }
    public void ChangeTheLifeOfTheUser (int life) {
        _imageToAffect.fillAmount =  (float)(life) /100f;

    }
	
	// Update is called once per frame
	
}
